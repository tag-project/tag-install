
# Instruction v1.4.0:
* [Requirements](#requirements)
* [Installation Instructions for TAG Application](#installation-instructions-for-tag-application)
* [Update Instructions for TAG Application](#update-instructions-for-tag-application)


## Requirements:

-   Docker must be installed.
    
-   Docker Compose must be installed.
    
-   Application images should be available on the host where the installation will be performed.

## Installation Instructions for TAG Application:

1. Install docker and docker-compose to the server
<https://docs.docker.com/engine/install/>

2.  Upload tag-back-1.tar.xz, tag-front-1.tar.xz, tag-worker-1.tar.xz to the server
> Example :
> 
> scp -i ~/.ssh/tag-auto-deploy tag-front/tag-front-1.tar.xz tag_borys@54.77.173.196:/{upload_folder}
> 
> scp -i ~/.ssh/tag-auto-deploy tag-worker/tag-worker-1.tar.xz tag_borys@54.77.173.196:/{upload_folder}
> 
> scp -i ~/.ssh/tag-auto-deploy tag-back/tag-back-1.tar.xz tag_borys@54.77.173.196:/{upload_folder}
>
> scp -i ~/.ssh/tag-auto-deploy tag-back/tag-ws-1.tar.xz tag_borys@54.77.173.196:/{upload_folder}

3.  Load back image: `sudo docker load < /{upload_folder}/tag-back-1.tar.xz`

![enter image description here](./screenshots/back-install.png)

4. Load front image: `sudo docker load < /{upload_folder}/tag-front-1.tar.xz`

![enter image description here](./screenshots/front-install.png)

5. Load worker image: `sudo docker load < /{upload_folder}/tag-worker-1.tar.xz`

![enter image description here](./screenshots/worker-install.png)

6. Load worker image: `sudo docker load < /{upload_folder}/tag-ws-1.tar.xz`

![enter image description here](./screenshots/ws-install.png)

7. Download installation script:  
`git clone https://gitlab.com/tag-project/tag-install.git`

![enter image description here](./screenshots/clone-insall-script.png)

8. Prepare installation file: `cd tag-install/ && chmod +x run.sh`  

![enter image description here](./screenshots/prepare-install-file.png)

9. Run installation file: `sudo ./run.sh`

![enter image description here](./screenshots/run-script.png)

10. Enter Your IP or Domain:

![enter image description here](./screenshots/enter-domain.png)

11. Enter the protocol (https or http) you will use when logging into the UI. If you will use IP please specify to use http: 

![enter image description here](./screenshots/choose-protocol.png)

12. Result

![enter image description here](./screenshots/domain-result.png)

13. Enter Your PostgreSQL IP (If you don't have a preset base, press enter):

![enter image description here](./screenshots/pg-connect.png)

4. Installation done

![enter image description here](./screenshots/installation-done.png)

15. Please copy the "**HID**" code and share it with the Trigger API Gateway team. This code is required by the team to generate a license file for you. Please provide the HID to your Trigger Software manager to proceed with obtaining the license.

![enter image description here](./screenshots/hid.png)

16. For installing license, please use the curl request with your license file and server:

> Command:
> `curl -F upload=@{path to the "license.key" file} http://{your IP or domain}/internal/license`
> 
> Example:
> `curl -F upload=@license.key http://13.58.28.136/internal/license`
> 
> Note: Please make sure to adjust the protocol type (HTTP or HTTPS)
> according to the one you have set up. This ensures that the command
> works correctly, especially when dealing with URLs starting with
> "https://" or "http://".


## <a id="update">Update Instructions from v1.3.0 for TAG Application:</a>
1. Stop containers {-DON'T STOP DB CONTAINER-}
`sudo docker stop tag-install_tag_front_1 tag-install_tag_back_1  tag-install_tag_worker_1`
![enter image description here](./screenshots/stop-containers.png)

2. Delete containers {-DON'T DELETE DB CONTAINER-}
`sudo docker rm tag-install_tag_front_1 tag-install_tag_back_1  tag-install_tag_worker_1`
![enter image description here](./screenshots/delete-containers.png)

3. Delete images {-DON'T DELETE DB IMAGE-}
`sudo docker rmi tag-back-1 tag-worker-1 tag-front-1`
![enter image description here](./screenshots/delete-images.png)

4.  Upload tag-back-1.tar.xz, tag-front-1.tar.xz, tag-worker-1.tar.xz to the server
> Example :
> 
> scp -i ~/.ssh/tag-auto-deploy tag-front/tag-front-1.tar.xz tag_borys@54.77.173.196:/{upload_folder}
> 
> scp -i ~/.ssh/tag-auto-deploy tag-worker/tag-worker-1.tar.xz tag_borys@54.77.173.196:/{upload_folder}
> 
> scp -i ~/.ssh/tag-auto-deploy tag-back/tag-back-1.tar.xz tag_borys@54.77.173.196:/{upload_folder}
>
> scp -i ~/.ssh/tag-auto-deploy tag-back/tag-ws-1.tar.xz tag_borys@54.77.173.196:/{upload_folder}

5.  Load back image: `sudo docker load < /{upload_folder}/tag-back-1.tar.xz`
![enter image description here](./screenshots/back-install.png)

6. Load front image: `sudo docker load < /{upload_folder}/tag-front-1.tar.xz`
![enter image description here](./screenshots/front-install.png)

7. Load worker image: `sudo docker load < /{upload_folder}/tag-worker-1.tar.xz`
![enter image description here](./screenshots/worker-install.png)

8. Load worker image: `sudo docker load < /{upload_folder}/tag-ws-1.tar.xz`

![enter image description here](./screenshots/ws-install.png)

9. Update installation script:  
`git pull https://gitlab.com/tag-project/tag-install.git`
`git checkout main`

![enter image description here](./screenshots/clone-insall-script.png)

10. Prepare installation file: `cd tag-install/ && chmod +x run.sh`  

![enter image description here](./screenshots/prepare-install-file.png)

11. Run installation file: `sudo ./run.sh`

![enter image description here](./screenshots/run-script.png)

12. Enter Your IP or Domain:

![enter image description here](./screenshots/enter-domain.png)

13. Enter the protocol (https or http) you will use when logging into the UI. If you will use IP please specify to use http: 

![enter image description here](./screenshots/choose-protocol.png)

14. Result

![enter image description here](./screenshots/domain-result.png)

15. Enter Your PostgreSQL IP (If you don't have a preset base, press enter):

![enter image description here](./screenshots/pg-connect.png)

16. Installation done

![enter image description here](./screenshots/installation-done.png)

17. Call migration url to make db migration
`https://<host>/internal/migrate/130-140`

