#!/bin/bash

read -p "Enter Your IP or Domain: "  mydomain

if [[ -z "${mydomain}" ]]; then
  mydomain=`ip route get 1 | sed -n 's/^.*src \([0-9.]*\) .*$/\1/p'`
fi

read -p "Enter the protocol (https or http) you will use when logging into the UI. If you will use IP please specify to use http: "  mydomainprotocol

if [[ -z "${mydomainprotocol}" ]]; then
  mydomainprotocol=`echo http`
fi

echo "You entered domain(ip) -  $mydomainprotocol://$mydomain"

sed -i -e 's|.*FRONT_URL=.*|FRONT_URL='$mydomainprotocol'://'"$mydomain"'|g' .env

read -p "Enter Your PostgreSQL IP (If you don't have a preset base, press enter): "  dbhost

if [[ -z "${dbhost}" ]]; then
  dbhost=`ip route get 1 | sed -n 's/^.*src \([0-9.]*\) .*$/\1/p'`
  lic_host=`ip route get 1 | sed -n 's/^.*src \([0-9.]*\) .*$/\1/p'`
  sed -i -e 's|.*DB_HOST=.*|DB_HOST='"$dbhost"'|g' .env
  sed -i -e 's|.*DB_USER=.*|DB_USER=tag_user|g' .env
  sed -i -e 's|.*DB_PASSWORD=.*|DB_PASSWORD=TAGPASSWDDB|g' .env
  sed -i -e 's|.*POSTGRES_USER:.*|      POSTGRES_USER: tag_user|g' docker-compose-db.yaml
  sed -i -e 's|.*POSTGRES_PASSWORD:.*|      POSTGRES_PASSWORD: TAGPASSWDDB|g' docker-compose-db.yaml
  sed -i -e 's|.*DB_HOST_STAT=.*|DB_HOST_STAT='"$dbhost"'|g' .env
  sed -i -e 's|.*DB_USER_STAT=.*|DB_USER_STAT=tag_user|g' .env
  sed -i -e 's|.*DB_PASSWORD_STAT=.*|DB_PASSWORD_STAT=TAGPASSWDDB|g' .env
  sed -i -e 's|.*DB_NAME_STAT=.*|DB_NAME_STAT=timescaledb|g' .env
  sed -i -e 's|.*POSTGRES_DB:.*|      POSTGRES_DB: timescaledb|g' docker-compose-db.yaml
  sed -i -e 's|.*REDIS_HOST=.*|REDIS_HOST='"$dbhost"'|g' .env
  sed -i -e 's|.*LIC_SERVER=.*|LIC_SERVER='"$lic_host:8585"'|g' .env
  docker network create database
  docker network create tag_front
  docker-compose -f docker-compose-db.yaml up -d
  docker-compose -f docker-compose.yaml up -d
  sleep 5
  hid=`curl http://127.0.0.1:9191/internal/license 2>/dev/null`
  echo "Your HID: $hid "
  echo "for start -> $mydomainprotocol://$mydomain"
  exit 0
fi

sed -i -e 's|.*DB_HOST=.*|DB_HOST='"$dbhost"'|g' .env

lic_host=`ip route get 1 | sed -n 's/^.*src \([0-9.]*\) .*$/\1/p'`
sed -i -e 's|.*LIC_SERVER=.*|LIC_SERVER='"$lic_host:8585"'|g' .env

read -p "Enter Your Redis IP (If you don't have a preset base, press enter): "  redisdbhost

if [[ -z "${redisdbhost}" ]]; then
  redisdbhost=`ip route get 1 | sed -n 's/^.*src \([0-9.]*\) .*$/\1/p'`
  sed -i -e 's|.*REDIS_HOST=.*|REDIS_HOST='"$redisdbhost"'|g' .env
  docker network create database
  docker-compose -f docker-compose-db.yaml up -d redisdb
fi

sed -i -e 's|.*REDIS_HOST=.*|REDIS_HOST='"$redisdbhost"'|g' .env

read -p "Enter Your PostgreSQL User: "  dbuser

if [[ -z "${dbuser}" ]]; then
  dbuser=`echo tag_user`
fi

sed -i -e 's|.*DB_USER=.*|DB_USER='"$dbuser"'|g' .env
sed -i -e 's|.*POSTGRES_USER:.*|      POSTGRES_USER: '"$dbuser"'|g' docker-compose-db.yaml

read -p "Enter Your PostgreSQL User Password: "  dbpass

if [[ -z "${dbpass}" ]]; then
  dbpass=`echo TAGPASSWDDB`
fi

sed -i -e 's|.*DB_PASSWORD=.*|DB_PASSWORD='"$dbpass"'|g' .env
sed -i -e 's|.*POSTGRES_PASSWORD:.*|      POSTGRES_PASSWORD: '"$dbpass"'|g' docker-compose-db.yaml

### Statistics PostgreSQL

read -p "Enter Your Statistics PostgreSQL IP (If you don't have a preset base, press enter): "  dbhoststat

if [[ -z "${dbhoststat}" ]]; then
  dbhoststat=`ip route get 1 | sed -n 's/^.*src \([0-9.]*\) .*$/\1/p'`
fi

sed -i -e 's|.*DB_HOST_STAT=.*|DB_HOST_STAT='"$dbhoststat"'|g' .env

read -p "Enter Your Statistics PostgreSQL User: "  dbuserstat

if [[ -z "${dbuserstat}" ]]; then
  dbuserstat=`echo timescaledb`
fi

sed -i -e 's|.*DB_USER_STAT=.*|DB_USER_STAT='"$dbuserstat"'|g' .env

read -p "Enter Your Statistics PostgreSQL User Password: "  dbpassstat

if [[ -z "${dbpassstat}" ]]; then
  dbpassstat=`echo timescaledbtagpassword`
fi

sed -i -e 's|.*DB_PASSWORD_STAT=.*|DB_PASSWORD_STAT='"$dbpassstat"'|g' .env

read -p "Enter Your Statistics PostgreSQL Database Name (If you don't have a preset, press enter): "  dbnamestat

if [[ -z "${dbnamestat}" ]]; then
  dbnamestat=`echo timescaledb`
fi

sed -i -e 's|.*DB_NAME_STAT=.*|DB_NAME_STAT='"$dbnamestat"'|g' .env

read -p "Enter Your Statistics PostgreSQL Port (default is 5433): "  dbportstat

if [[ -z "${dbportstat}" ]]; then
  dbportstat=5433
fi

sed -i -e 's|.*DB_PORT_STAT=.*|DB_PORT_STAT='"$dbportstat"'|g' .env

###

docker network create tag_front
docker-compose -f docker-compose.yaml up -d

sleep 5
hid=`curl http://127.0.0.1:9191/internal/license 2>/dev/null`
echo "Your HID: $hid "

echo "for start -> $mydomainprotocol://$mydomain"
